const { response } = require('express');
const ProductModel = require('./product.model');

function map_product_req(product, productDetails) {
    if (productDetails.name)
        product.name = productDetails.name;
    if (productDetails.description)
        product.description = productDetails.description;
    if (productDetails.brand)
        product.brand = productDetails.brand;
    if (productDetails.category)
        product.category = productDetails.category;
    if (productDetails.price)
        product.price = productDetails.price;
    if (productDetails.color)
        product.color = productDetails.color;
    if (productDetails.size)
        product.size = productDetails.size;
    if (productDetails.quantity)
        product.quantity = productDetails.quantity;
    if (productDetails.tags)
        product.tags = typeof (productDetails.tags) === 'string' ? productDetails.tags.split(',') : productDetails.tags;
    if (productDetails.warrentyStatus)
        product.warrentyStatus = productDetails.warrentyStatus;
    if (productDetails.warrentyPeroid)
        product.warrentyPeroid = productDetails.warrentyPeroid;
    if (productDetails.returnEligible)
        product.returnEligible = productDetails.returnEligible;
    if (productDetails.returnTimeFromPurchase)
        product.returnTimeFromPurchase = productDetails.returnTimeFromPurchase;
    if (productDetails.offers)
        product.offers = typeof (productDetails.offers) === 'string' ? productDetails.offers.split(',') : productDetails.offers;
    if (productDetails.returnTimeFromPurchase)
        product.returnTimeFromPurchase = productDetails.returnTimeFromPurchase;
    if (productDetails.images)
        product.images = productDetails.images;
    if (productDetails.vendor)
        product.vendor = Object.values(productDetails.vendor).length ? productDetails.vendor._id : productDetails.vendor
    if (productDetails.status)
        product.status = productDetails.status;
    if (productDetails.discountedItem || productDetails.discounteType || productDetails.discountValue) {
        if (!product.discount)
            product.discount = {};
        if (productDetails.discountedItem)
            product.discount.discountedItem = productDetails.discountedItem;
        if (productDetails.discountType)
            product.discount.discountType = productDetails.discountType;
        if (productDetails.discountValue)
            product.discount.discountValue = productDetails.discountValue;
    }

    if (productDetails.ratingMsg && productDetails.ratingPoint) {
        var ratings = {
            point: productDetails.ratingPoint,
            message: productDetails.ratingMsg,
            user: productDetails.user
        };
        product.ratings.push(ratings);

    }
}

function save(data) {
    let newProduct = new ProductModel({});
    map_product_req(newProduct, data);
    return newProduct.save();
}

function find(condtion, skipCount = 0, pageSize = 100) {
    return ProductModel
        .find(condtion)
        .limit(pageSize)
        .skip(skipCount)
        .sort({
            _id: -1
        })
        .populate('vendor', { username: 1, email: 1, role: 1 })
}

function update(id, data) {
    return new Promise(function (resolve, reject) {

        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err);
            }
            if (!product) {
                return reject({
                    msg: "Product Not Found",
                    status: 404
                })
            }
            var oldImages = product.images;

            map_product_req(product, data);
            product.save(function (err, updated) {
                if (err) {
                    return reject(err);
                }
                resolve({
                    oldImages: oldImages,
                    updatedProduct: updated
                });
            })
        })
    })

}

function remove(id) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err);
            }
            if (!product) {
                return reject({
                    msg: "Product Not Found",
                    status: 404
                })
            }
            product.remove(function (err, done) {
                if (err) {
                    return reject(err);
                }
                resolve(done);
            })
        })
    })
}

module.exports = {
    save,
    find,
    update,
    remove,
    map_product_req
}
const ProductQuery = require('./product.query');
const fs = require('fs');
const path = require('path');
function get(req, res, next) {
    var condition = {};
    const currentPage = (req.query.pageNumber || 1) - 1;
    const pageSize = Number(req.query.pageSize || 100);
    const skipCount = Number(pageSize * currentPage);
    // if admin give all the products if not give the product added by current user
    if (req.user.role !== 1) {
        condition.vendor = req.user._id;
    }
    ProductQuery
        .find(condition, skipCount, pageSize)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function getById(req, res, next) {
    var condition = { _id: req.params.id };
    ProductQuery
        .find(condition)
        .then(function (data) {
            res.json(data[0]);
        })
        .catch(function (err) {
            next(err);
        })
}

function post(req, res, next) {
    const data = req.body;
    if (req.fileTypeErr) {
        return next({
            msg: 'Invalid File Format',
            status: 400
        })
    }
    if (req.file) {
        data.images = req.file.filename;
    }
    if (req.files) {
        data.images = req.files.map(function (file) {
            return file.filename;
        })
    }
    // todo prepare data
    data.vendor = req.user._id;

    ProductQuery
        .save(data)
        .then(function (done) {
            res.json(done);
        })
        .catch(function (err) {
            next(err);
        })
}

function update(req, res, next) {
    const data = req.body;
    data.user = req.user._id;
    if (req.fileTypeErr) {
        return next({
            msg: 'Invalid File Format',
            status: 400
        })
    }
    if (req.file) {
        data.images = req.file.filename;
    }
    ProductQuery
        .update(req.params.id, data)
        .then(function (response) {
            if (req.file) {
                fs.unlink(path.join(process.cwd(), 'uploads/' + response.oldImages[0]), function (err, done) {
                    if (err) {
                        console.log('file removed failed', err);
                    } else {
                        console.log('file removed')
                    }
                })
            }

            res.json(response.updatedProduct);
        })
        .catch(function (err) {
            next(err);
        })

}

function remove(req, res, next) {
    ProductQuery
        .remove(req.params.id)
        .then(function (data) {
            res.json(data)
        })
        .catch(function (err) {
            next(err);
        })
}

function search(req, res, next) {
    var data;
    if (req.method === 'POST') {
        data = req.body;
    }
    if (req.method === 'GET') {
        data = req.query
    }
    var searchCondition = {};
    ProductQuery.map_product_req(searchCondition, data);
    if (data.minPrice) {
        searchCondition.price = {
            $gte: data.minPrice
        }
    }
    if (data.maxPrice) {
        searchCondition.price = {
            $lte: data.maxPrice
        }
    }
    if (data.minPrice && data.maxPrice) {
        searchCondition.price = {
            $gte: data.minPrice,
            $lte: data.maxPrice
        }
    }

    if (data.fromDate && data.toDate) {
        const fromDate = new Date(data.fromDate).setHours(0, 0, 0, 0); // returns miliseconds
        const toDate = new Date(data.toDate).setHours(23, 59, 59, 999); // returns miliseconds

        searchCondition.createdAt = {
            $gte: new Date(fromDate),
            $lte: new Date(toDate)
        }
    }

    if (data.tags) {
        searchCondition.tags = {
            $in: data.tags.split(',')
        }
    }

    if (data.offers) {
        searchCondition.offers = {
            $in: data.offers.split(',')
        }
    }

    console.log('search condition >>', searchCondition)
    ProductQuery
        .find(searchCondition)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

module.exports = {
    get,
    getById,
    post,
    update,
    remove,
    search
}
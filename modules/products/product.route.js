const router = require('express').Router();
const ProductController = require('./product.controller');
const Authenticate = require('./../../middlewares/authenticate');
const Uploader = require('./../../middlewares/uploader');

router.route('/')
    .get(Authenticate, ProductController.get)
    .post(Authenticate, Uploader.array('image'), ProductController.post);

router.route('/search')
    .get(ProductController.search)
    .post(ProductController.search);

router.route('/delete')
    .get(Authenticate, ProductController.remove)

router.route('/:id')
    .get(Authenticate, ProductController.getById)
    .put(Authenticate, Uploader.single('image'), ProductController.update)
    .delete(Authenticate, ProductController.remove);



module.exports = router;


const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RatingSchema = new Schema({
    point: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
})

const ProductSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    vendor: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    brand: String,
    price: Number,
    size: String,
    color: String,
    images: [String],
    description: String,
    warrentyStatus: Boolean,
    warrentyPeroid: String,
    returnEligible: {
        type: Boolean,
        default: true
    },
    returnTimeFromPurchase: String,
    quantity: Number,
    ratings: [RatingSchema],
    status: {
        type: String,
        enum: ['available', 'booked', 'out of stock', 'sold'],
        default: 'available'
    },
    offers: [String],
    tags: [String],
    discount: {
        discountedItem: Boolean,
        discountType: {
            type: String,
            enum: ['percentage', 'quantity', 'value']
        },
        discountValue: String
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('product', ProductSchema);
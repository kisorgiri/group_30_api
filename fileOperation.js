const fs = require('fs');

function myWrite(name, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./files/' + name, content, function (err, done) {
            if (err) {
                reject(err);
            }
            else {
                resolve(done);
            }
        })
    })
}


function myRead(fileName, cb) {
    fs.readFile('./files/' + fileName, 'UTF-8', function (err, done) {
        if (err) {
            cb(err);
        }
        else {
            cb(null, done);
        }
    })
}


// rename
// fs.rename('./files/kishor.txt', './files/new.txt', function (err, done) {
//     if (err) {
//         console.log('rename failed')

//     }
//     else {
//         console.log('renamed')
//     }
// })

// TODO create afunction and export it


// fs.unlink('./files/new.txt', function (err, done) {
//     if (err) {
//         console.log('err in removing >>', err);
//     }
//     else {
//         console.log('sucess in removing', done)
//     }
// })

// module.exports.write = myWrite;
// module.exports.read = myRead;

module.exports = {
    read: myRead,
    write: myWrite
}
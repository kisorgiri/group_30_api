const router = require('express').Router();
const UserModel = require('./../models/user.model');
const MapUser = require('./../helpers/map_user_req');
const Uploader = require('./../middlewares/uploader');

router.route('/')
    .get(function (req, res, next) {
        //    fetch all users
        // projection is inclusion or exclusion
        console.log('req.user >>', req.user);
        var condition = {};
        UserModel
            .find(condition, { password: 0 })
            .sort({
                _id: -1
            })
            // .limit(2)
            // .skip(2)
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.json(users);
            })

    })
    .post(function (req, res, next) {

    });



router.route('/search')
    .get(function (req, res, next) {
        res.json({
            msg: "from user search"
        })
    })
    .post(function (req, res, next) {

    })
    .put(function (req, res, next) {

    })
    .delete(function (req, res, next) {

    });

router.route('/:id')
    .get(function (req, res, next) {
        console.log('req.user >>', req.user);
        //    get by id
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User not found",
                    status: 404
                })
            }
            res.json(user);
        })
    })
    .put(Uploader.single('img'), function (req, res, next) {
        console.log('req.body >', req.body);
        if (req.fileTypeErr) {
            return next({
                msg: "Invalid File Format",
                status: 400
            })
        }

        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'User not found',
                    status: 404
                })
            }
            let data = req.body;
            if (req.file) {
                data.image = req.file.filename;
            }
            // user is mongoose object
            var newUpdatedMappedUser = MapUser(user, data)


            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                // if new file is uplaoded remove old one
                res.json(done);
            })
        })

    })
    .delete(function (req, res, next) {
        console.log('req.user >>', req.user);
        if (req.user.role !== 1) {
            return next({
                msg: "You dont have access",
                status: 403
            })
        }
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User not found",
                    status: 404
                })
            }
            user.remove(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })
    });



module.exports = router;
const express = require('express');
const router = express.Router(); // routing level middleware
const UserModel = require('./../models/user.model');
const MapUser = require('./../helpers/map_user_req');
const Uploader = require('./../middlewares/uploader');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../configs');
const nodemailer = require('nodemailer');
const randomStr = require('randomstring');

const sender = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'broadwaytest44@gmail.com',
        pass: 'Broadwaytest44!'
    }
})

function prepareEmail(data) {
    const emailBody = {
        from: '"Group30 Store" <noreply@somthing.com>', // sender address
        to: "gautam.tamang0000@gmail.com," + data.email, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `<p>
        Hi <strong>${data.name}</strong>,</p>
        <p>We noticed that you are having trouble logging into our system. please click the link below to reset your password</p>
        <p><a href="${data.link}" target="_blank">Click here to reset password</a></p>
        <p>If you have not requested to change your password please kindly ignore this email</p>
        <p>Regards,</p>
        <p>Group30 Store Support Team</p>
        `, // html body
    }

    return emailBody
}

function generateToken(data) {
    let token = jwt.sign({
        _id: data._id
    }, config.JWT_Secret);
    return token;
}

router.get('/', function (req, res, next) {
    require('fs').readFile('sdlfkjsadlf.dkf', function (err, done) {
        if (err) {
            req.myEvent.emit('err', err, res);
        }
    })
})

router.post('/login', function (req, res, next) {
    UserModel
        .findOne({
            $or: [
                { username: req.body.username },
                { email: req.body.username }
            ]
        })
        .then(function (user) {
            if (user) {
                var isMatched = passwordHash.verify(req.body.password, user.password);
                if (isMatched) {
                    // token genration
                    let token = generateToken(user);
                    res.json({
                        user: user,
                        token: token
                    })

                } else {
                    next({
                        msg: "Invalid password",
                        status: 400
                    })
                }
            } else {
                next({
                    msg: "Invalid username",
                    status: 400
                })
            }
        })
        .catch(function (err) {
            next(err);
        })
})

router.post('/register', Uploader.single('img'), function (req, res, next) {
    let data = req.body;

    // 
    // const mimetype = req.file.mimetype.split('/')[0];
    // if (mimetype !== 'image') {
    //     // remove the uploaded file from server
    //     // TODO remove uploaded file if it is not of correct format
    //     return next({
    //         msg: "Invalid file format",
    //         status: 400
    //     })
    // }
    // 
    if (req.fileTypeErr) {
        return next({
            msg: "Invalid File Format",
            status: 400
        })
    }
    if (req.file) {

        data.image = req.file.filename;
    }

    let newUser = new UserModel({}); //mongoose object
    // TODO run query to find with username and email to know if the value is available
    var newMappedUser = MapUser(newUser, req.body);

    newUser.password = passwordHash.generate(req.body.password);
    newUser.save(function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done);
    })


})
router.post('/forgot-password', function (req, res, next) {
    UserModel.findOne({
        email: req.body.email
    })
        .then(function (user) {
            if (!user) {
                return next({
                    msg: "Email address not registered yet",
                    status: 400
                })
            }
            // user is available
            const passwordResetExpiry = new Date().getTime() + (1000 * 60 * 60 * 24);
            const passwordResetToken = randomStr.generate(23);


            // now send email
            var emailData = {
                name: user.username,
                email: user.email,
                link: `${req.headers.origin}/reset_password/${passwordResetToken}`
            }

            user.passwordResetExpiry = passwordResetExpiry;
            user.passwordResetToken = passwordResetToken;
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                sender.sendMail(prepareEmail(emailData), function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.json(done);
                })
            })
        })
        .catch(function (err) {
            next(err);
        })
})

router.post('/reset-password/:token', function (req, res, next) {

    UserModel.findOne({
        passwordResetToken: req.params.token,
        passwordResetExpiry: {
            $gte: Date.now()
        }
    })
        .then(function (user) {
            if (!user) {
                return next({
                    msg: "Invalid/Expired reset password token",
                    status: 400
                })
            }
            // check expiry

            user.password = passwordHash.generate(req.body.password);
            user.passwordResetToken = null;
            user.passwordResetExpiry = null;
            user.save(function (err, updated) {
                if (err) {
                    return next(err);
                }
                res.json(updated);
            })
        })
        .catch(function (err) {
            next(err);
        })
})

module.exports = router;

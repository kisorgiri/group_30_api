// http
// http server - http client

// http server ===> nodejs and express
// ===> mobile desktop
// http client ===> react


const abc = require('http');
const fileOp = require('./fileOperation');

const server = abc.createServer(function (req, res) {
    // callback function for every http client request
    // regardless of any http url or http method this callback will be executed

    // req res cycle must be completed
    // http is stateless protocol
    // req will contain entire http request object
    // res will contain entire http respnose object
    // res object is responsible for sending response from server
    // res.end();
    console.log('req.url >>', req.url);
    console.log('req.method >>', req.method)
    if (req.url === '/write') {
        fileOp.write('random.txt', 'test something')
            .then(function (data) {
                res.end('file writing success');
            })
            .catch(function (err) {
                res.end('file writing failed')
            })
    }
    else if (req.url === '/read') {
        fileOp.read('random.txt', function (err, done) {
            if (err) {
                res.end('file reading failed');
            }
            else {
                res.end('file reading success >>' + done);
            }
        })
    } else {
        res.end('no any actions to perform')
    }
})

server.listen(9090, function (err, done) {
    if (err) {
        console.log('err in listening');
    }
    else {
        console.log('server listening at port 9090');
        console.log('press CTRL + C to exit');
    }
})
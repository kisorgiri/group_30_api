const express = require('express');
const morgan = require('morgan');
const path = require('path');
const app = express(); /// app is entire express framework
const cors = require('cors');
app.set('port', 9090);

// run db.js file as part of app.js file
require('./db');

// event stuff
require('./socket')(app);
const events = require('events');
const myEvent = new events.EventEmitter();

myEvent.on('err', function (error, res) {
    console.log('at err event handler >', error);
    res.status(400).json(error);
})

app.use(function (req, res, next) {
    req.myEvent = myEvent;
    next();
})

// import routing level middleware
const APIRoute = require('./routes/api.routes');

// load third party middleware
app.use(morgan('dev'));
app.use(cors()); // accept all request

// inbuilt middleware
// serve static files
app.use(express.static('files')) // internal serving within express application
app.use('/file', express.static(path.join(__dirname, 'uploads'))) // external client serve
// parse incoming data
// server will have different data parser as per content-type
// parase incoming x-www-formurlencoded data

app.use(express.urlencoded({
    extended: true
}))
// json parser
app.use(express.json());



// routing config
app.use('/api', APIRoute)

// 404 error not found handler
app.use(function (req, res, next) {
    next({
        msg: "Not Found",
        status: 404
    })
})

// error handling middleware
// to get executed error handling middleware must be called
// next(with_argument);
// argument sent on next will be err (1st placeholder value)
app.use(function (err, req, res, next) {
    console.log('error handling middleware', err)
    // TODO set response status code
    res.status(err.status || 400)
    res.json({
        msg: err.msg || err,
        status: err.status || 400
    })
})

app.listen(app.get('port'), function (err, done) {
    if (err) {
        console.log('err in listening');
    } else {
        console.log('server listening at port ' + app.get('port'));
        console.log('press CTRL +C to exit');
    }
});

// middleware

// function(){

// }

// middleware is a function that has access to http request object http response object and next middleware function reference
// middleware can modify http request and response
// middleware always came into action in between request response cycle
// the order of middleware is very important

// syntax
// function(req,res,next){
    // req or 1st argument is for http request object
    // res or 2nd argument is for http response object
    // next or 3rd argument is for next middleware reference
// }

// configuration block
// app.use() .use method is used to add middlewares


// types of middleware
// 1. application level middleware
// 2. routing level middleware
// 3. third party middleware
// 4. inbuilt middleware
// 5. error handling middlware
module.exports = function (user, userDetails) {
    if (userDetails.name)
        user.name = userDetails.name;
    if (userDetails.username)
        user.username = userDetails.username;
    if (userDetails.password)
        user.password = userDetails.password;
    if (userDetails.email)
        user.email = userDetails.email;
    if (userDetails.phoneNumber)
        user.phoneNumber = userDetails.phoneNumber;
    if (!user.address)
        user.address = {};
    if (userDetails.tempAddress)
        user.address.tempAddress = userDetails.tempAddress.split(',');
    if (userDetails.permanentAddress)
        user.address.permanentAddress = userDetails.permanentAddress;
    if (userDetails.gender)
        user.gender = userDetails.gender;
    if (userDetails.dob)
        user.dateOfBirth = userDetails.dob;
    if (userDetails.isMarried)
        user.isMarried = userDetails.isMarried;
    if (userDetails.role)
        user.role = userDetails.role
    if (userDetails.status)
        user.status = userDetails.status;
    if (userDetails.image)
        user.image = userDetails.image;

    return user;
}
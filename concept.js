// NODE js is run time environment for JavaScript on server

// additional tool we will get after installation of nodejs are
// NPM ==> Javascript pacakge manager
// NPX ==> package management tool

// npm command

// npm init  ==> it will intialize a JS project with creating a package.json file

// package.json is project main intrductory file which holds overall project details
// try to maintain updated information in package.json file

// install pacakges from npmjs
// command
// npm install <pacakge_name> //package_name must match the name in npmjs.com

// install the packages,it's dependency tree in node_Modules folder
// it will update pacakge.json file with dependency

// npm install ==> it install all the dependecy from pacakge.json file

//uninstall pacakges from npmjs

// npm uninstall <pacakge_name>


// file-file communication
// es6

// es5 syntax
// one file must export and one file must import
// export syntax
// module.exports = DataType

// import syntax
// const abc = require('./twitter_lit/app.js')


// REST API

// REST (REPRESENTATIONAL STATE TRANSFER)
// following points needs to be fullfilled to be on REST
// 1. Stateless
// 2. correct use of http verb
// GET ==> fetch
// PUT/PATCH ==> update
// DELETE ===> remove
// POST==> insert
// 3. data must be in either XML or JSON
// 4.caching of GET request


// API(Application programming interface)
// PL=>
// APP==> APP
// combination of method and url eg POST /Login
const router = require('express').Router();
const authRouter = require('./../controllers/auth.controller');
const userRouter = require('./../controllers/user.controller');
const productRouter = require('./../modules/products/product.route');
// applicatio level middleware
const authneticate = require('./../middlewares/authenticate');


router.use('/auth', authRouter);
router.use('/user', authneticate, userRouter);
router.use('/product', productRouter);


module.exports = router;
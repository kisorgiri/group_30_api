const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {
        type: String,
        trim: true
    },
    phoneNumber: {
        type: Number
    },
    email: {
        type: String,
        unique: true,
        sparse: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    address: {
        tempAddress: [String],
        permanentAddress: String
    },
    dateOfBirth: Date,
    image: String,
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },
    role: {
        type: Number,// 1 for admin, 2 for end user, 3 visitor
        default: 2
    },
    status: {
        type: String,
        enum: ['active', 'inactive'],
        default: 'active'
    },
    isMarried: Boolean,
    passwordResetExpiry: Date,
    passwordResetToken: String
}, {
    timestamps: true
})


const UserModel = mongoose.model('user', UserSchema);

module.exports = UserModel;
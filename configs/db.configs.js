const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const conxnURL = 'mongodb://localhost:27017';
const dbName = 'group30db';
const Oid = mongodb.ObjectID;

module.exports = {
    MongoClient,
    conxnURL,
    dbName,
    Oid
}
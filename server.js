const http = require('http');

const server = http.createServer(function (request, response) {
    // request or 1st argument is always http request object
    // response of 2nd argument is always http response object
    console.log('client connected to server');
    // this callback function is executed for every clent call
    // console.log('request object >>', request);
    response.end('Welcome to Nodejs Server')
    console.log('request url >>', request.url)
    console.log('request method >>', request.method)

    // http protocol
    // http verbs(methods) GET ,PUT, POST , DELETE
    // http url '/login'
    // http statusCode
    // http request object
    // http response object

    // if(req.url === '/write'){

    // }

    // regardless of http method and url this callback is executed for every client request

})

server.listen(8080, 'localhost', function (err, done) {
    if (err) {
        console.log('error in listening >', err);
    }
    else {
        console.log('server listening at port 8080');
        console.log('press CTRL + C to exit');
    }
});

module.exports = function (req, res, next) {
    if (req.query.ticket === 'root') {
        next();
    }
    else {
       next({
            msg: 'Invalid Ticket'
        })
    }
}
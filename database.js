// database 
// container which holds data
// database management system
// ==>

// 2 types of DBMS
// 1. Relational database management system
// 2. distributed database management system

// 1.Relational DBMS
// E-R diagaram
// a.Table Based Design
// eg LMS ==> books, users,notification,reviews
// b.tuple or row for each record
// c.Schema based solution
//eg books {
    // name:String,
    // price:Number,
    // issueDate:''
// }
// d.non-scabale 
// e.Relation between table exists
// f. SQL database
// g. mysql, sql-lite,.....


// distributed DBMS
// a.COllection based approach
// users, reviews, books, notifications LMS example
// b.document based design each record is a document
// document can be any valid JSON object
// c.Schema Less design
// d.higly scalable
// e.Realtion doesnot exist
// f. NO SQL (not only sql)
// g.redis, mongodb, dynamodb, couchdb, cosmos db

// mongodb
// mongodb must be installed on our machine
// try creating an account on mongodb atlas for cloud storage

// command
// mongod ==> ensure driver is running (mongod --port <port>)
// mongodb server is up and running 
// default port of mongodb 27017

// to access mongo shell (mongo);
// we will get > interface to run shell command

// shell command
// show dbs  ==> shows all available database
// use <db_name> if(db_name exists) select existing database else create new database and select it
// db ==> show selected db
// show collections ==> show all avalilable collection of the  selected database

// CRUD

// Create
// db.<collection_name>.insert({})
// db.<collection_name>.insertMany([])

// READ
// db.<collection_name>.find({query_builder});
// db.<collection_name>.find({query_builder}).pretty() // formatted view
// db.<collection_name>.find({query_builder}).count();
// db.<collection_name>.find({query_builder}).sort().limit().skip();
// projection another object after query builder with inclusion and exclusion

// UPDATE
// db.<collection_name>.update({},{},{});
// first object ==> query builder
// 2nd object is payload section where key must be $set
// eg {$set:{object to be updated}}
// 3rd options, muti upsert

// REMOVE
// db.<collection_name>.remove({query_builder});
// note do not left the query builder empty

// drop collection
// db.<collection_name>.drop():

// drop database
// db.dropDatabase();

// ODM==> Object Document modelling
// mongoose

// advantanges of using mongoose
// 1. Schema Based solution
// 2.indexing is easy eg( required , unique ...)
// 3. datatype validation for properties(attributes)
// 4. middleware 
// 5.method


// DATABASE BACKUP & RESTORE
// mongodump & mongorestore
// mongoexport & mongoimport

// two ways 
// 1 bson structure
// 2 csv/json structure

// 1 bson
// command
// backup command mongodb
// mongodump ==> it takes backup of every data base inside default dump folder in bson structure
// mongodump --db<db_name> it takes backup of selected database only
// mongdump --db<db_name> --out <path_to_destination_folder>

// restore commadn mongorestore
// mongorestore ==> it search for bson backup inside default dump folder to backup
// mongorestore --drop  it will drop the existing document with same _id
// mongorestore <path_to_source_folder> restore from desired location

//csv and JSON

// json
// backup command mongoexport
// mongoexport --db <db_name> --collection <collection_name> --out <path_to_destination_with.csv_extension>
// mongoexport -d <db_name> -c <collection_name> -o <path_to_destination_with.csv_extension>

// import 
// command mongoimport
// mongoimport --db <existing_db or new_db_name> --collection <existing or new collection> <path_to_source_json_file>

// csv
// backup command mongoexport
// mongoexport --db<db_name> --collection <collection_name> --type=csv --fileds="comma,seperated propety to be exported" --out <path_to_destination_with.csv_extension>
// mongoexport --db<db_name> --collection <collection_name> --query="{key:'value'}" --type=csv --fileds="comma,seperated propety to be exported" --out <path_to_destination_with.csv_extension>

// import command
// mongoimport --db <db_name> --collection<collection_name> --type=csv <path_to_source> --headerline






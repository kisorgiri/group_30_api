module.exports = function (req, res, next) {
    if (req.query.admin === 'true') {
        next();
    }
    else {
        next({
            msg: 'You dont have access'
        })
    }
}
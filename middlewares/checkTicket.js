module.exports = function (req, res, next) {
    if (req.query.ticket) {
        next();
    }
    else {
        next({
            msg: 'You dont have ticket',
            status: 400
        })
    }
}
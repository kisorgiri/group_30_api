const multer = require('multer');
const path = require('path');

const diskStorage = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    },
    destination: function (req, file, cb) {
        cb(null, path.join(process.cwd(), 'uploads'))
    }
})

function imageFilter(req, file, cb) {
    const mimetype = file.mimetype.split('/')[0];
    if (mimetype === 'image') {
        cb(null, true)
    } else {
        req.fileTypeErr = true;
        cb(null, false);
    }
}

const upload = multer({
    storage: diskStorage,
    fileFilter: imageFilter
})

module.exports = upload;

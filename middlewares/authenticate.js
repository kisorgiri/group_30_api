const jwt = require('jsonwebtoken');
const config = require('./../configs');
const UserModel = require('./../models/user.model');

module.exports = function (req, res, next) {

    let token;
    if (req.headers['authorization'])
        token = req.headers['authorization']
    if (req.headers['x-access-token'])
        token = req.headers['x-access-token']
    if (req.headers['token'])
        token = req.headers['token']
    if (req.query['token'])
        token = req.query['token']

    if (token) {

        jwt.verify(token, config.JWT_Secret, function (err, decoded) {
            if (err) {
                return next(err);
            }
            UserModel.findById(decoded._id, function (err, user) {
                if (err) {
                    return next(err);
                }
                if (!user) {
                    return next({
                        msg: "User removed from system",
                        status: 404
                    })
                }
                req.user = user;
                next();
            })
        })

    } else {
        next({
            msg: "Authentication Failed! Token not provided",
            status: 400
        })
    }
}
const socket = require('socket.io');
const config = require('./configs')

module.exports = function (app) {
    const io = socket(app.listen(config.SocketPort), {
        cors: {
            accept: '/*'
        }
    });
    io.on('connection', function (client) {
        console.log('socket client connected to server');
        client.emit('welcome', 'welcome to server');
        client.on('hello', function (data) {
            console.log('at heloo', data);
        })
    })
}